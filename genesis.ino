/****************************************************************
 * project : genesis
 * target  : TeensyLC
 * license : GNU GPLv3
 * author  : https://gitlab.com/qynn
 *
 * USB Genesis Controller
 *
 * Select 'Serial+Keyboard+Mouse+Joystick' from 'Tools > USB Type' in Arduino IDE
 * https://www.pjrc.com/teensy/td_joystick.html
 *
 ****************************************************************/
#define DEBUG 0
#define LED_PIN 13

#define NUM_PINS 8
// button IDs based on joy.cpl layout
static const uint8_t btns[NUM_PINS] = {8, 13, 14, 15, // start, A, B, C,
                                       9, 11, 2, 18};    // left, right, up, down,

// pinmap
static const uint8_t pins[NUM_PINS] = {17, 16, 21, 18,
                                       19, 23, 22, 20};

void setup(){

  Serial.begin(115200);
  if(DEBUG){
    while(!Serial){}
    Serial.println("Serial Ready!");
  }

  for(int i=0; i<NUM_PINS; i++){
    pinMode(pins[i], INPUT); //external pullups
  }

  pinMode(LED_PIN, OUTPUT);

}

void loop(){

  uint8_t s[NUM_PINS];
  uint8_t b = 0;

  for(int i=0; i<NUM_PINS; i++){
    s[i] = !digitalRead(pins[i]);
    b |= s[i];
    Joystick.button(btns[i], s[i]);
    if(DEBUG){
      Serial.printf("| %i ", s[i] ? btns[i] : 0);
    }
  }

  if(DEBUG){
    Serial.printf("|| %i\n", b);
  }

  // turn on led if any button pressed
  digitalWrite(LED_PIN, b);

  delay(DEBUG ? 500 : 50);

}
